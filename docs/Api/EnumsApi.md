# Ensi\OmsClient\EnumsApi

All URIs are relative to *http://localhost/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createRefundReason**](EnumsApi.md#createRefundReason) | **POST** /refunds/refund-reasons | Создание объекта типа RefundReason
[**getRefundReasons**](EnumsApi.md#getRefundReasons) | **GET** /refunds/refund-reasons | Получение объекта типа RefundReason
[**patchRefundReason**](EnumsApi.md#patchRefundReason) | **PATCH** /refunds/refund-reasons/{id} | Обновление части полей объекта типа RefundReason



## createRefundReason

> \Ensi\OmsClient\Dto\RefundReasonResponse createRefundReason($create_refund_reason_request)

Создание объекта типа RefundReason

Создание объекта типа RefundReason

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\OmsClient\Api\EnumsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$create_refund_reason_request = new \Ensi\OmsClient\Dto\CreateRefundReasonRequest(); // \Ensi\OmsClient\Dto\CreateRefundReasonRequest | 

try {
    $result = $apiInstance->createRefundReason($create_refund_reason_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling EnumsApi->createRefundReason: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **create_refund_reason_request** | [**\Ensi\OmsClient\Dto\CreateRefundReasonRequest**](../Model/CreateRefundReasonRequest.md)|  | [optional]

### Return type

[**\Ensi\OmsClient\Dto\RefundReasonResponse**](../Model/RefundReasonResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## getRefundReasons

> \Ensi\OmsClient\Dto\RefundReasonsResponse getRefundReasons()

Получение объекта типа RefundReason

Получение объекта типа RefundReason

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\OmsClient\Api\EnumsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);

try {
    $result = $apiInstance->getRefundReasons();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling EnumsApi->getRefundReasons: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

This endpoint does not need any parameter.

### Return type

[**\Ensi\OmsClient\Dto\RefundReasonsResponse**](../Model/RefundReasonsResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## patchRefundReason

> \Ensi\OmsClient\Dto\RefundReasonResponse patchRefundReason($id, $patch_refund_reason_request)

Обновление части полей объекта типа RefundReason

Обновление части полей объекта типа RefundReason

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\OmsClient\Api\EnumsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Числовой id
$patch_refund_reason_request = new \Ensi\OmsClient\Dto\PatchRefundReasonRequest(); // \Ensi\OmsClient\Dto\PatchRefundReasonRequest | 

try {
    $result = $apiInstance->patchRefundReason($id, $patch_refund_reason_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling EnumsApi->patchRefundReason: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Числовой id |
 **patch_refund_reason_request** | [**\Ensi\OmsClient\Dto\PatchRefundReasonRequest**](../Model/PatchRefundReasonRequest.md)|  |

### Return type

[**\Ensi\OmsClient\Dto\RefundReasonResponse**](../Model/RefundReasonResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)

