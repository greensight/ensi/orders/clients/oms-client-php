# # ShipmentReadonlyProperties

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | идентификатор | 
**number** | **string** | номер отгрузки | 
**delivery_id** | **int** | ид отправления | 
**store_id** | **int** | ид склада | 
**status_at** | [**\DateTime**](\DateTime.md) | дата установки статуса | 
**cost** | **int** | сумма (в копейках) товаров отгрузки (рассчитывается автоматически) | 
**width** | **int** | ширина (рассчитывается автоматически) | 
**height** | **int** | высота (рассчитывается автоматически) | 
**length** | **int** | длина (рассчитывается автоматически) | 
**weight** | **int** | вес (рассчитывается автоматически) | 
**created_at** | [**\DateTime**](\DateTime.md) | дата создания | 
**updated_at** | [**\DateTime**](\DateTime.md) | дата обновления | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


