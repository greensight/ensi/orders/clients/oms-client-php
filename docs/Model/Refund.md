# # Refund

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | идентификатор | 
**price** | **int** | сумма возврата, коп. | 
**is_partial** | **bool** | является заявкой на частичный возврат | 
**created_at** | [**\DateTime**](\DateTime.md) | дата создания | 
**updated_at** | [**\DateTime**](\DateTime.md) | дата обновления | 
**status** | **int** | статус заявки на возврат из RefundStatusEnum | [optional] 
**responsible_id** | **int** | идентификатор ответственного | [optional] 
**rejection_comment** | **string** | причина отклонения | [optional] 
**order_id** | **int** | идентификатор заказа | [optional] 
**manager_id** | **int** | идентификатор администратора (если автором был администратор) | [optional] 
**source** | **int** | источник взаимодействия (канал) | [optional] 
**user_comment** | **string** | комментарий пользователя | [optional] 
**order** | [**\Ensi\OmsClient\Dto\Order**](Order.md) |  | [optional] 
**items** | [**\Ensi\OmsClient\Dto\OrderItem[]**](OrderItem.md) |  | [optional] 
**reasons** | [**\Ensi\OmsClient\Dto\RefundReason[]**](RefundReason.md) |  | [optional] 
**files** | [**\Ensi\OmsClient\Dto\RefundFile[]**](RefundFile.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


