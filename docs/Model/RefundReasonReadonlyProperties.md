# # RefundReasonReadonlyProperties

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | идентификатор | 
**created_at** | [**\DateTime**](\DateTime.md) | дата создания | 
**updated_at** | [**\DateTime**](\DateTime.md) | дата обновления | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


