# # Delivery

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | ID отправления | 
**order_id** | **int** | ID заказа | 
**number** | **string** | номер отправления | 
**status_at** | [**\DateTime**](\DateTime.md) | дата установки статуса | 
**cost** | **int** | себестоимость доставки, полученная от службы доставки в копейках | 
**width** | **int** | ширина | 
**height** | **int** | высота | 
**length** | **int** | длина | 
**weight** | **int** | вес | [optional] 
**created_at** | [**\DateTime**](\DateTime.md) | дата создания | 
**updated_at** | [**\DateTime**](\DateTime.md) | дата обновления | 
**date** | [**\DateTime**](\DateTime.md) | желаемая дата доставки | [optional] 
**timeslot** | [**\Ensi\OmsClient\Dto\Timeslot**](Timeslot.md) |  | [optional] 
**status** | **int** | статус отправления из DeliveryStatusEnum | [optional] 
**shipments** | [**\Ensi\OmsClient\Dto\Shipment[]**](Shipment.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


