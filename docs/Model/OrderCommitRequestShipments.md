# # OrderCommitRequestShipments

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**store_id** | **int** | Склад, с которого идет отгрузка | 
**items** | [**\Ensi\OmsClient\Dto\OrderCommitRequestItems[]**](OrderCommitRequestItems.md) | Офферы, попавшие в эту отгрузку | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


