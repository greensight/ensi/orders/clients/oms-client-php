<?php
/**
 * PaymentStatusEnum
 *
 * PHP version 5
 *
 * @category Class
 * @package  Ensi\OmsClient
 * @author   OpenAPI Generator team
 * @link     https://openapi-generator.tech
 */

/**
 * OMS
 *
 * Управление заказами
 *
 * The version of the OpenAPI document: 1.0.0
 * Contact: mail@greensight.ru
 * Generated by: https://openapi-generator.tech
 * OpenAPI Generator version: 4.3.1
 */

/**
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */

namespace Ensi\OmsClient\Dto;
use \Ensi\OmsClient\ObjectSerializer;

/**
 * PaymentStatusEnum Class Doc Comment
 *
 * @category Class
 * @description Статус оплаты. Расшифровка значений:   * &#x60;1&#x60; - Не оплачен   * &#x60;2&#x60; - Средства захолдированы   * &#x60;100&#x60; - Оплачен   * &#x60;101&#x60; - Возврат   * &#x60;200&#x60; - Просрочен   * &#x60;201&#x60; - Отменен
 * @package  Ensi\OmsClient
 * @author   OpenAPI Generator team
 * @link     https://openapi-generator.tech
 */
class PaymentStatusEnum
{
    
    /** Не оплачена */
    public const NOT_PAID = 1;
    
    /** Средства захолдированы */
    public const HOLD = 2;
    
    /** Оплачен */
    public const PAID = 100;
    
    /** Возврат */
    public const RETURNED = 101;
    
    /** Просрочен */
    public const TIMEOUT = 200;
    
    /** Отменен */
    public const CANCELED = 201;
    
    /**
     * Gets allowable values of the enum
     * @return string[]
     */
    public static function getAllowableEnumValues(): array
    {
        return [
            self::NOT_PAID,
            self::HOLD,
            self::PAID,
            self::RETURNED,
            self::TIMEOUT,
            self::CANCELED,
        ];
    }

    /**
    * Gets allowable values and titles of the enum
    * @return string[]
    */
    public static function getDescriptions(): array
    {
        return [
            self::NOT_PAID => 'Не оплачена',
            self::HOLD => 'Средства захолдированы',
            self::PAID => 'Оплачен',
            self::RETURNED => 'Возврат',
            self::TIMEOUT => 'Просрочен',
            self::CANCELED => 'Отменен',
        ];
    }
}


